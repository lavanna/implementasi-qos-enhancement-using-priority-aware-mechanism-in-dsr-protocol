# ======================================================================
# Define options
# ======================================================================

#Set all the options

set val(chan)         Channel/WirelessChannel  ;# channel type
set val(prop)         Propagation/TwoRayGround ;# radio-propagation model
set val(ant)          Antenna/OmniAntenna      ;# Antenna type
set val(ll)           LL                       ;# Link layer type
set val(ifq)          Queue/DropTail/PriQueue  ;# Interface queue type
set val(ifqlen)       50                       ;# max packet in ifq
set val(netif)        Phy/WirelessPhy          ;# network interface type
set val(mac)          Mac/802_11               ;# MAC type
set val(rp)           DSR                     ;# routing protocol
set val(x)            500                      ;# X length
set val(y)            500                      ;# Y length
set val(finish)       60                       ;# Finish time
set val(nn)           100                      ;# number of mobilenodes
set val(seed)               1.0                ;# seed cbr
set val(cp)                 "session"          ;#<-- traffic file
set val(sc)                 "mobility"              ;#<-- mobility file

if { $val(rp) == "DSR" } {
set val(ifq) CMUPriQueue
} else {
set val(ifq) Queue/DropTail/PriQueue
} 

set ns_ [new Simulator]

set f [open out.tr w]
$ns_ trace-all $f

set namtrace [open out.nam w]
$ns_ namtrace-all-wireless $namtrace $val(x) $val(y)

set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)
 
set god_ [create-god $val(nn)]
set chan_1 [new $val(chan)]

# CONFIGURE AND CREATE NODES

$ns_ node-config -adhocRouting      $val(rp) \
                 -llType            $val(ll) \
                 -macType           $val(mac) \
                 -ifqType           $val(ifq) \
                 -ifqLen            $val(ifqlen) \
                 -antType           $val(ant) \
                 -propType          $val(prop) \
                 -phyType           $val(netif) \
                 -topoInstance      $topo \
                 -agentTrace        ON \
                 -routerTrace       ON \
                 -macTrace          ON \
                 -movementTrace     ON \
                 -channel           $chan_1

Phy/WirelessPhy set RXThresh_ 3.65262e-10 ; #250m receiver sensitivity range
Phy/WirelessPhy set CSThresh_ 3.65262e-10 ; #250m capture threshold  range

# disable random motion
for {set i 0} {$i < $val(nn)} {incr i} {
    set node_($i) [$ns_ node]
    $node_($i) random-motion 0 ;
}


proc finish {} {
    global ns_ namtrace filename
    $ns_ flush-trace
    close $namtrace  
    exec nam out.nam &
    exit 0
}

source mobility
    $ns_ at 0.0 "$node_(1) color blue"
    $node_(1) color "blue"
    $ns_ at 0.0 "$node_(1) label \"source\""

    $ns_ at 0.0 "$node_(99) color blue"
    $node_(99) color "blue"
    $ns_ at 0.0 "$node_(99) label \"destination\""


# Define node movement model
puts "Loading connection pattern..."
source $val(cp)

# Define traffic model
puts "Loading scenario file..."
source $val(sc)

# Define node initial position in nam
for {set i 0} {$i < $val(nn)} {incr i} {

    # 20 defines the node size in nam, must adjust it according to your scenario
    # The function must be called after mobility model is defined
    
    $ns_ initial_node_pos $node_($i) 20
}

# Tell nodes when the simulation ends
for {set i 0} {$i < $val(nn) } {incr i} {
    $ns_ at $val(finish).0 "$node_($i) reset";
}

puts $f "M 0.0 nn $val(nn) x $val(x) y $val(y) rp $val(rp)"
puts $f "M 0.0 sc $val(sc) cp $val(cp) seed $val(seed)"
puts $f "M 0.0 prop $val(prop) ant $val(ant)"

$ns_ at $val(finish) "finish"
puts "Start of simulation..."
$ns_ run

