# Implementasi QoS enhancement using priority aware mechanism in DSR protocol

Meningkatkan QoS pada protokol DSR dengan menggunakan menerapkan priority aware.
Simulasi Parameter :
1. Waktu Simulasi : 60s
2. Topology Area  : 500mx500m
3. Banyak Node    : 100
4. Mobility speed  : 2 - 20 m/s
5. Packet Size       : 1000
dll.